#!/usr/bin/env bash

function echo_black() { echo "$(tput setaf 0)${*}$(tput sgr0)"; }
function echo_blue() { echo "$(tput setaf 4)${*}$(tput sgr0)"; }
function echo_cyan() { echo "$(tput setaf 6)${*}$(tput sgr0)"; }
function echo_green() { echo "$(tput setaf 2)${*}$(tput sgr0)"; }
function echo_lime_yellow() { echo "$(tput setaf 190)${*}$(tput sgr0)"; }
function echo_magenta() { echo "$(tput setaf 5)${*}$(tput sgr0)"; }
function echo_powder_blue() { echo "$(tput setaf 153)${*}$(tput sgr0)"; }
function echo_red() { echo "$(tput setaf 1)${*}$(tput sgr0)"; }
function echo_yellow() { echo "$(tput setaf 3)${*}$(tput sgr0)"; }
