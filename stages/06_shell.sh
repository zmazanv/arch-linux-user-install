#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 6

# Set login shell.
function _set_shell() {
	local __ZSH_BIN='/bin/zsh'

	if [[ -x "${__ZSH_BIN}" ]]; then
		chsh -s /bin/zsh
		echo_green 'Set ZSH as login shell!'
	else
		echo_magenta 'ZSH does not appear to be installed. Bash will remain the login shell for the user.'
	fi

	return 0
}
