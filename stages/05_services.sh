#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 5

# Enable systemd user services.
function _enable_services() {
	local __DIRPATH='./resources/services'
	local __CUSTOM_FILEPATH="${__DIRPATH}/custom.srvc"
	local __SYSTEM_FILEPATH="${__DIRPATH}/system.srvc"
	local -a __CUSTOM_SERVICES
	local -a __SYSTEM_SERVICES

	function _fetch_custom_services() {
		[[ ! -f "${__CUSTOM_FILEPATH}" ]] && return 1

		readarray -t __CUSTOM_SERVICES < <(uniq "${__CUSTOM_FILEPATH}")

		[[ "${#__CUSTOM_SERVICES[@]}" -eq 0 ]] && return 1

		return 0
	}

	function _fetch_system_services() {
		[[ ! -f "${__SYSTEM_FILEPATH}" ]] && return 1

		readarray -t __SYSTEM_SERVICES < <(uniq "${__SYSTEM_FILEPATH}")

		[[ "${#__SYSTEM_SERVICES[@]}" -eq 0 ]] && return 1

		return 0
	}

	function _enable() {
		local __autostart_target='autostart.target'

		if _fetch_custom_services; then
			systemctl --user add-wants "${__autostart_target}" "${__CUSTOM_SERVICES[@]}"
			echo_green 'Enabled custom systemd user services!'
		else
			echo_magenta "No custom systemd user services to enable. Skipping..."
		fi

		if _fetch_system_services; then
			systemctl --user enable "${__SYSTEM_SERVICES[@]}"
			echo_green 'Enabled system systemd user services!'
		else
			echo_magenta "No system systemd user services to enable. Skipping..."
		fi

		return 0
	}

	_enable

	unset -f _fetch_custom_services
	unset -f _fetch_system_services
	unset -f _enable
	return 0
}
