#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 2

# Install packages.
function _install_packages() {
	local __DIRPATH='./resources/packages'
	local __AUR_DIRPATH="${__DIRPATH}/aur"
	local __FLATPAK_DIRPATH="${__DIRPATH}/flatpak"
	local __NATIVE_DIRPATH="${__DIRPATH}/native"
	local __NIX_DIRPATH="${__DIRPATH}/nix"
	local -a __AUR_PKGS
	local -a __FLATPAK_PKGS
	local -a __NATIVE_PKGS
	local -a __NIX_PKGS

	if [[ ! -d "${__DIRPATH}" ]]; then
		echo_red "'${__DIRPATH}' not found. Exiting..."
		exit 2
	fi

	# Set up Nixpkgs channel.
	function _nix_setup() {
		command -v nix >/dev/null 2>&1 || return 1

		nix-channel --add 'https://nixos.org/channels/nixpkgs-unstable'
		nix-channel --update

		echo_green 'Added and updated Nixpkgs channel!'
		return 0
	}

	# Set rustup toolchain.
	function _rustup_setup() {
		command -v rustup >/dev/null 2>&1 || return 1

		rustup default stable

		echo_green "Set rustup toolchain to 'stable'!"
		return 0
	}

	# Parse the packages to install.
	function _fetch_pkgs() {
		local -a __aur_pkgs
		local -a __flatpak_pkgs
		local -a __native_pkgs
		local -a __nix_pkgs

		if [[ ! -d "${__AUR_DIRPATH}" ]]; then
			echo_magenta "'${__AUR_DIRPATH}' not found. No AUR packages to install. Skipping..."
		else
			readarray -t __aur_pkgs < <(
				find "${__AUR_DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-execdir cat {} + |
					uniq
			)

			if [[ "${#__aur_pkgs[@]}" -eq 0 ]]; then
				echo_red "No packages were found in '${__AUR_DIRPATH}'. Skipping..."
			else
				__AUR_PKGS=("${__aur_pkgs[@]}")
			fi
		fi

		if [[ ! -d "${__FLATPAK_DIRPATH}" ]]; then
			echo_magenta "'${__FLATPAK_DIRPATH}' not found. No flatpak packages to install. Skipping..."
		else
			readarray -t __flatpak_pkgs < <(
				find "${__FLATPAK_DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-execdir cat {} + |
					uniq
			)

			if [[ "${#__flatpak_pkgs[@]}" -eq 0 ]]; then
				echo_red "No packages were found in '${__FLATPAK_DIRPATH}'. Skipping..."
			else
				__FLATPAK_PKGS=("${__flatpak_pkgs[@]}")
			fi
		fi

		if [[ ! -d "${__NATIVE_DIRPATH}" ]]; then
			echo_magenta "'${__NATIVE_DIRPATH}' not found. No native packages to install. Skipping..."
		else
			readarray -t __native_pkgs < <(
				find "${__NATIVE_DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-execdir cat {} + |
					uniq
			)

			if [[ "${#__native_pkgs[@]}" -eq 0 ]]; then
				echo_red "No packages were found in '${__NATIVE_DIRPATH}'. Skipping..."
			else
				__NATIVE_PKGS=("${__native_pkgs[@]}")
			fi
		fi

		if [[ ! -d "${__NIX_DIRPATH}" ]]; then
			echo_magenta "'${__NIX_DIRPATH}' not found. No nix packages to install. Skipping..."
		else
			readarray -t __nix_pkgs < <(
				find "${__NIX_DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-execdir cat {} + |
					uniq
			)

			if [[ "${#__nix_pkgs[@]}" -eq 0 ]]; then
				echo_red "No packages were found in '${__NIX_DIRPATH}'. Skipping..."
			else
				__NIX_PKGS=("${__nix_pkgs[@]/#/'nixpkgs.'}")
			fi
		fi

		return 0
	}

	# Install packages interactively.
	function _install_pkgs() {
		if [[ -n "${__NATIVE_PKGS[*]}" ]]; then
			while true; do
				echo_yellow 'Interactive native package installation starting in 3 seconds...'
				sleep 3
				if sudo pacman -S --needed "${__NATIVE_PKGS[@]}"; then
					echo_green 'Finished native package installation!'
					break
				fi

				while true; do
					echo_magenta 'Native package installation failed.'
					echo_blue 'Would you like to rerun it? (y/n):'
					read -r
					case "${REPLY}" in
					'n' | 'N')
						echo_red 'Exiting...'
						exit 2
						;;
					'y' | 'Y')
						break
						;;
					esac
				done
			done
		fi

		if [[ -n "${__AUR_PKGS[*]}" ]]; then
			while true; do
				echo_yellow 'Interactive AUR package installation starting in 3 seconds...'
				sleep 3
				if paru -S --needed "${__AUR_PKGS[@]}"; then
					echo_green 'Finished AUR package installation!'
					break
				fi

				while true; do
					echo_magenta 'AUR package installation failed.'
					echo_blue 'Would you like to rerun it? (y/n):'
					read -r
					case "${REPLY}" in
					'n' | 'N')
						echo_red 'Exiting...'
						exit 2
						;;
					'y' | 'Y')
						break
						;;
					esac
				done
			done
		fi

		if _nix_setup && [[ -n "${__NIX_PKGS[*]}" ]]; then
			while true; do
				echo_yellow 'Nix package installation starting in 3 seconds...'
				sleep 3
				if nix-env -iA "${__NIX_PKGS[@]}"; then
					echo_green 'Finished nix package installation!'
					break
				fi

				while true; do
					echo_magenta 'Nix package installation failed.'
					echo_blue 'Would you like to rerun it? (y/n):'
					read -r
					case "${REPLY}" in
					'n' | 'N')
						echo_red 'Exiting...'
						exit 2
						;;
					'y' | 'Y')
						break
						;;
					esac
				done
			done
		fi

		if [[ -n "${__FLATPAK_PKGS[*]}" ]]; then
			while true; do
				echo_yellow 'Interactive Flatpak package installation starting in 3 seconds...'
				sleep 3
				if sudo flatpak install flathub "${__FLATPAK_PKGS[@]}"; then
					echo_green 'Finished AUR package installation!'
					break
				fi

				while true; do
					echo_magenta 'Flatpak package installation failed.'
					echo_blue 'Would you like to rerun it? (y/n):'
					read -r
					case "${REPLY}" in
					'n' | 'N')
						echo_red 'Exiting...'
						exit 2
						;;
					'y' | 'Y')
						break
						;;
					esac
				done
			done
		fi

		echo_green 'Finished package installation!'
		return 0
	}

	# Build the cache for tldr.
	function _build_tldr_cache() {
		command -v tldr >/dev/null 2>&1 || return 1

		tldr -u

		echo_green 'Generated the local cache for TLDR!'
		return 0
	}

	_fetch_pkgs
	_install_pkgs
	_nix_setup
	_rustup_setup
	_build_tldr_cache

	unset -f _fetch_pkgs
	unset -f _install_pkgs
	unset -f _nix_setup
	unset -f _rustup_setup
	unset -f _build_tldr_cache
}
