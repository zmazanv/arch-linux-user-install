#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 3

# Make adjustments to home directory.
function _set_up_home() {
	# Rename the XDG user dir for videos if the locale is mexican spanish.
	function _rename_mexican_xdg_videos_dir() {
		local __xdg_dirs="${HOME}/.config/user-dirs.dirs"

		[[ "${LANG}" != 'es_MX.UTF-8' ]] && return 0
		[[ ! -d "${HOME}/Vídeos" ]] && return 0

		mv "${HOME}/Vídeos" "${HOME}/Videos"

		[[ -f "${__xdg_dirs}" ]] && sed -Ei 's/Vídeos/Videos/' "${__xdg_dirs}"

		return 0
	}

	function _create_projects_dir() {
		case "${LANG}" in
		en_*)
			mkdir -p "${HOME}/Projects"
			;;
		es_*)
			mkdir -p "${HOME}/Proyectos"
			;;
		esac

		return 0
	}

	function _enable_dark_theme() {
		gsettings set org.gnome.desktop.interface color-scheme prefer-dark

		echo_green 'Set preference for GTK dark theme!'
		return 0
	}

	_rename_mexican_xdg_videos_dir
	_create_projects_dir
	_enable_dark_theme

	unset -f _rename_mexican_xdg_videos_dir
	unset -f _create_projects_dir
	unset -f _enable_dark_theme
	return 0
}
