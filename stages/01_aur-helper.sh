#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 1

# Install AUR helper. Paru is used here.
function _install_aur_helper() {
	local __paru_dir='/tmp/paru-bin'

	while true; do
		[[ -d "${__paru_dir}" ]] && rm -rf "${__paru_dir}"

		if git clone 'https://aur.archlinux.org/paru-bin.git' "${__paru_dir}"; then
			break
		fi
	done

	(
		if cd "${__paru_dir}"; then
			makepkg -si
		else
			exit 1
		fi
	)

	rm -rf "${__paru_dir}"

	echo 'Installed paru!'
	return 0
}
