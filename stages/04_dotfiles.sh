#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 4

# Fetch and install dotfiles.
function _fetch_dotfiles() {
	local __DOTFILES_URL='https://gitlab.com/zmazanv/dotfiles.git'
	local __DOTFILES_GIT_DIR="${HOME}/.dotfiles"
	local __DOTFILES_TEMP_DIR
	__DOTFILES_TEMP_DIR="$(mktemp -d)"

	# Git wrapper to use dotfiles git dir and $HOME worktree.
	function _git_dotfiles() {
		git --git-dir="${__DOTFILES_GIT_DIR}" --work-tree="${HOME}" "$@"
	}

	while true; do
		rm -rf "${__DOTFILES_TEMP_DIR}"
		if git clone "${__DOTFILES_URL}" "${__DOTFILES_TEMP_DIR}"; then
			break
		fi
	done

	cp -r "${__DOTFILES_TEMP_DIR}/.git" "${__DOTFILES_GIT_DIR}"

	_git_dotfiles restore "${HOME}"
	_git_dotfiles config --local status.showUntrackedFiles no
	_git_dotfiles remote set-url origin 'git@gitlab.com:zmazanv/dotfiles.git'

	rm -rf "${__DOTFILES_TEMP_DIR}"

	echo_green 'Fetched and installed dotfiles!'

	unset -f _git_dotfiles_alias
	return 0
}
