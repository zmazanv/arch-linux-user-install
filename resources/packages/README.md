# Packages

These are just text files with Arch, AUR, and Flatpak package names listed.

They are used by `_install_packages` in `../stages/02_packages.sh`.
