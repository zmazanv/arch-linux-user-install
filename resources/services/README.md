# Services

These are just text files with systemd units listed.

They are used by `_enable_services` in `../stages/05_services.sh`.
